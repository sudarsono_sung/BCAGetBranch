package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

public class DeviceManagementAdapter {
	final static Logger logger = Logger.getLogger(DeviceManagementAdapter.class);

	public static Integer CheckWSID(String SerialNumber, String WSID){
		model.mdlLog mdlLog = new model.mdlLog();
		mdlLog.ApiFunction = "getBranchList";
		mdlLog.SystemFunction = "CheckWSID";
		mdlLog.LogSource = "Webservice";
		mdlLog.SerialNumber = SerialNumber;
		mdlLog.WSID = WSID;
		mdlLog.LogStatus = "Failed";
		mdlLog.ErrorMessage = "";

		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		Integer returnValue = 0;
		try{
			//define connection
			connection = database.RowSetAdapter.getConnectionWL();
			String sql = "SELECT SerialNumber FROM ms_device WHERE WSID = ?";
			pstm = connection.prepareStatement(sql);
			//insert sql parameter
			pstm.setString(1, WSID);
			//execute query
			jrs = pstm.executeQuery();
			while(jrs.next()){
				String SerialNumberFromDB = jrs.getString("SerialNumber");
				//if serial number is not same, then return error
				if (!SerialNumber.equalsIgnoreCase(SerialNumberFromDB)){
					returnValue = 1;
				}
			}
			mdlLog.LogStatus = "Success";
		}
		catch(Exception ex) {
			returnValue = 2;
			logger.error("FAILED. API : BCAGetBranch, method : GET, function : CheckWSID, Exception : " + ex.toString(), ex);
			mdlLog.ErrorMessage = ex.toString();
			LogAdapter.InsertLog(mdlLog);
		}
		finally {
			try {
				//close the opened connection
				 if (pstm != null) pstm.close();
				 if (connection != null) connection.close();
				 if (jrs != null) jrs.close();
			}
			catch(Exception e) {

			}
		}
		return returnValue;
	}

	public static List<model.mdlBranch> GetBranchList(String branchCode, String SerialNumber, String WSID){
		List<model.mdlBranch> branchList = new ArrayList<model.mdlBranch>();

		model.mdlLog mdlLog = new model.mdlLog();
		mdlLog.ApiFunction = "getBranchList";
		mdlLog.ApiFunction = "GetBranchList";
		mdlLog.SystemFunction = "CheckBranchCode";
		mdlLog.LogSource = "Webservice";
		mdlLog.SerialNumber = SerialNumber;
		mdlLog.WSID = WSID;
		mdlLog.LogStatus = "Failed";
		mdlLog.ErrorMessage = "";

		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		try{
			//define connection
			connection = database.RowSetAdapter.getConnectionWL();

			//call store procedure
			String sql = "SELECT branch.BranchCode, branch.BranchName, branch.BranchTypeID, branchtype.BranchTypeName, branch.BranchInitial FROM ms_branch branch "
						+ "LEFT JOIN ms_branchtype branchtype ON branch.BranchTypeID = branchtype.branchtypeid "
						+ "WHERE BranchCode = ?";
			pstm = connection.prepareStatement(sql);
			//insert sql parameter
			pstm.setString(1, branchCode);
			//execute query
			jrs = pstm.executeQuery();
			while(jrs.next()){
				model.mdlBranch branch = new model.mdlBranch();
				branch.BranchCode = jrs.getString("BranchCode");
				branch.BranchName = jrs.getString("BranchName");
				branch.BranchTypeID = jrs.getString("BranchTypeID");
				branch.BranchTypeName = jrs.getString("BranchTypeName");
				branch.BranchInitial = jrs.getString("BranchInitial");
				branchList.add(branch);
			}
			mdlLog.LogStatus = "Success";
		}
		catch(Exception ex) {
			logger.error("FAILED. API : BCAGetBranch, method : GET, function : GetBranchList, Exception : " + ex.toString(), ex);
			mdlLog.ErrorMessage = ex.toString();
			LogAdapter.InsertLog(mdlLog);
		}
		finally {
			try {
				//close the opened connection
				 if (pstm != null) pstm.close();
				 if (connection != null) connection.close();
				 if (jrs != null) jrs.close();
			}
			catch(Exception e) {

			}
		}
		//do log

		return branchList;
	}

}