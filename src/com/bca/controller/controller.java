package com.bca.controller;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;

import adapter.DeviceManagementAdapter;
import adapter.LogAdapter;

@RestController
public class controller {

	final static Logger logger = Logger.getLogger(controller.class);

	@RequestMapping(value = "/ping",method = RequestMethod.POST,consumes="application/json",headers="Accept=application/json")
	public @ResponseBody void GetPing()
	{
		return;
	}

    @RequestMapping(value = "/get",method = RequestMethod.GET)
   	public @ResponseBody model.mdlAPIResult GetBranchList(@RequestParam(value="serial", defaultValue="") String SerialNumber,
   														  @RequestParam(value="wsid", defaultValue="") String WSID )
	{
		model.mdlAPIResult mdlGetBranchListResult = new model.mdlAPIResult();
		model.mdlErrorSchema mdlErrorSchema = new model.mdlErrorSchema();
		model.mdlMessage mdlMessage = new model.mdlMessage();

		model.mdlLog mdlLog = new model.mdlLog();
		mdlLog.WSID = WSID;
		mdlLog.SerialNumber = SerialNumber;
		mdlLog.ApiFunction = "getBranchList";
		mdlLog.SystemFunction = "GetBranchList";
		mdlLog.LogSource = "Webservice";
		mdlLog.LogStatus = "Failed";

		Integer checkWSID = 1;
		try {
			checkWSID = DeviceManagementAdapter.CheckWSID(SerialNumber, WSID);
			if (checkWSID == 1){
				mdlErrorSchema.ErrorCode = "01";
				mdlMessage.Indonesian = "WSID yang Anda input sudah terdaftar";
				mdlMessage.English = mdlLog.ErrorMessage = "Your WSID Input Already Registered";
				mdlErrorSchema.ErrorMessage = mdlMessage;
				mdlGetBranchListResult.ErrorSchema = mdlErrorSchema;
				LogAdapter.InsertLog(mdlLog);
				return mdlGetBranchListResult;
			}else if(checkWSID == 2){
				mdlErrorSchema.ErrorCode = "02";
				mdlMessage.Indonesian = "Pengecekan WSID gagal";
				mdlMessage.English = mdlLog.ErrorMessage = "WSID check failed";
				mdlErrorSchema.ErrorMessage = mdlMessage;
				mdlGetBranchListResult.ErrorSchema = mdlErrorSchema;
				LogAdapter.InsertLog(mdlLog);
				return mdlGetBranchListResult;
			}

			String branchCode = WSID.substring(0, 4);

			List<model.mdlBranch> branchList = DeviceManagementAdapter.GetBranchList(branchCode, SerialNumber, WSID);

			if (branchList.size() == 0){
				mdlErrorSchema.ErrorCode = "03";
				mdlMessage.Indonesian = "Cabang tidak ditemukan";
				mdlMessage.English = mdlLog.ErrorMessage = "Branch not found";
				mdlErrorSchema.ErrorMessage = mdlMessage;
				mdlGetBranchListResult.ErrorSchema = mdlErrorSchema;
				LogAdapter.InsertLog(mdlLog);
				return mdlGetBranchListResult;
			}else{
				mdlErrorSchema.ErrorCode = "00";
				mdlMessage.Indonesian = "Berhasil";
				mdlMessage.English = mdlLog.LogStatus = "Success";
				mdlErrorSchema.ErrorMessage = mdlMessage;
				mdlGetBranchListResult.ErrorSchema = mdlErrorSchema;
				mdlGetBranchListResult.OutputSchema = branchList;
			}
			logger.info("SUCCESS. API : BCAGetBranch, method : GET, serial:" + SerialNumber + ", WSID : " + WSID);
		}catch(Exception ex){
			mdlErrorSchema.ErrorCode = "04";
			mdlMessage.Indonesian = "Gagal memanggil service";
			mdlMessage.English = mdlLog.ErrorMessage = "Service call failed";
			mdlErrorSchema.ErrorMessage = mdlMessage;
			mdlGetBranchListResult.ErrorSchema = mdlErrorSchema;
			logger.error("FAILED. API : BCAGetBranch, method : GET, serial:" + SerialNumber
						+ ", WSID : " + WSID + ", Exception : " + ex.toString());
		}
		LogAdapter.InsertLog(mdlLog);
		return mdlGetBranchListResult;
	}

}
